﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerGenerals : MonoBehaviour{

    public Text dinero;
    public int torres;

    void Start(){
        Debug.Log(PlayerPrefs.HasKey("TorresAjedrez"));
        if (PlayerPrefs.HasKey("TorresAjedrez")) {
            torres = PlayerPrefs.GetInt("TorresAjedrez");
            UpdateMoney();
        } else {
            PlayerPrefs.SetInt("TorresAjedrez", 100);
            torres = PlayerPrefs.GetInt("TorresAjedrez");
            UpdateMoney();
            Save();
        }
    }
    void Update() {
        if (Input.GetKeyDown(KeyCode.M)) {
            Debug.Log(PlayerPrefs.HasKey("TorresAjedrez")+" "+torres.ToString());
        }
    }
    public void UpdateMoney() {
        dinero.text = torres.ToString();
    }

    public void Save() {
        PlayerPrefs.Save();
        Debug.Log("Dinero guardado, es: " + torres);
    }
}
