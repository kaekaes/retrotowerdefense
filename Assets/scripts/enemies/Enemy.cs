﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    [Header("Velocidades")]
    public float startSpeed = 1f;
    [HideInInspector]
    public float speed;

    //[Header("Movimiento")]
    private Transform target;
    private int wavepointIndex = 0;

    //Vida
    public int health = 10;
    //dinero que da
    public int moneytodrop = 50;
    [HideInInspector]
    public GameObject Manager_Obj;

    void Start() {
        Manager_Obj = GameObject.FindGameObjectWithTag("GameManager");
        //asigna su velocidad inicial (ya que puede ser modificada) y de target va al primer waypoint
        speed = startSpeed;
        target = Waypoints.points[0];
	}
	
    //moverse al waypoint y si está a X distancia cambiarlo al siguiente
	void Update () {
        if (!GameManager.gameEnded) {
            if (health <= 0) {
                Destroy(gameObject);
                PlayerStats.Money += moneytodrop;
            }
            Vector3 dir = target.position - transform.position;
            transform.Translate(dir.normalized * speed * Time.deltaTime, Space.World);

            //Si estás a X distancia ve al siguiente punto
            if (Vector3.Distance(transform.position, target.position) <= 0.1f) {
                GetNextWaypoint();
            }
            speed = startSpeed;
        }
	}

    //Avanzar al siguiente waypoint y si no existe terminar (destroy)
    void GetNextWaypoint() {
        if(wavepointIndex >= Waypoints.points.Length - 1) {
            EndPath();
            return;
        }
        wavepointIndex++;
        target = Waypoints.points[wavepointIndex];
        //WaveSpawner.EnemiesAlive--;
        if (Waypoints.points[wavepointIndex].position.x < Waypoints.points[wavepointIndex - 1].position.x) {
            this.GetComponent<SpriteRenderer>().flipX = true;
        } else {
            this.GetComponent<SpriteRenderer>().flipX = false;
        }
    }

    void EndPath() {
        //vidas del jugador --;
        //WaveSpawner.EnemiesAlive--;
        Destroy(gameObject);
        Manager_Obj.GetComponent<PlayerStats>().HitLives();
    }

}
