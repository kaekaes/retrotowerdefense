﻿using UnityEngine;

public class BuildManager : MonoBehaviour {

    //lo que hace es que desde cualquier script se pueda llegar a este mismo script (se llama singleton y es muy importante)
    public static BuildManager instance;

    void Awake() {
		
        if (instance != null){
            Debug.LogError("Más de un BuildManager!");
            return;
        }

        instance = this;
    }

	[Header("Torretas Mario Bros x64")]
    //objeto de la torreta de fuego
    public GameObject FireplantTurretPrefab;
    //objeto de la torreta de hielo
    public GameObject IceplantTurretPrefab;
    //objeto de la torreta pitey 
    public GameObject PiteyTurretPrefab;
    //objeto de la torreta bill 
    public GameObject BillTurretPrefab;

    //Almacena la torreta que se quiere construir
    private TurretBlueprint turretToBuild;

    //Esto es una propiedad, hace que unicamente devuelva valores, nunca se escribe en ella
    public bool CanBuild { get { return turretToBuild != null; } }
    public bool HasMoney { get { return PlayerStats.Money >= turretToBuild.cost; } }

    //Accion de construir
    public void BuildTurretHere (Node node) {

        if (PlayerStats.Money < turretToBuild.cost) {

            Debug.Log("No tienes dinero para construir esa torreta");
            return;
        }

        //como si tiene el dinero, se le resta
        PlayerStats.Money -= turretToBuild.cost;

        GameObject turret = (GameObject)Instantiate(turretToBuild.prefab, node.GetBuildPosition(), Quaternion.identity);
        node.turret = turret;


    }

    //se hace que la torreta que se tiene que constuir sea la que se pasa desde shop
    public void SelectTurretToBuild (TurretBlueprint turret) {
        turretToBuild = turret;
    }
}
