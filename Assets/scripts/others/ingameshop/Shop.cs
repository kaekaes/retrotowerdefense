﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop : MonoBehaviour {

    public List<TurretBlueprint> torres = new List<TurretBlueprint>();

    BuildManager buildManager;

    //La tienda debe ser reescaladada al crearse el juego para funcionar
    public RectTransform tamaniotienda;

    void Start() {
        buildManager = BuildManager.instance;
        tamaniotienda.sizeDelta = new Vector2(9000,tamaniotienda.sizeDelta.y);
    }

    //Se selecciona la torreta que se quiere colocar
    public void SelectTurret(string x) {
        //Aqui habrá que quitarle dinero
        foreach (var turr in torres) {
            if(turr.name == x) {
                buildManager.SelectTurretToBuild(turr);
            }
        }
    }
    /*
    public void SelectIcePlant() {
        buildManager.SelectTurretToBuild(IceplantTurret);
    }
    public void SelectPitey() {
        buildManager.SelectTurretToBuild(PiteyTurret);
    }
    public void SelectBill() {
        buildManager.SelectTurretToBuild(BillBalaTurret);
    }*/
}
[System.Serializable]
public class TurretBlueprint {

    public string name;
    public GameObject prefab;
    public int cost;

}
