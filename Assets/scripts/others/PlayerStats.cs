﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStats : MonoBehaviour {

    public static bool playing;

    public static int Money;
    public int startMoney = 500;

    public static int Lives;
    public int startLives = 10;
    public Text LivesTxt;

    public GameManager gameManager;

    void Start() {
        playing = true;
        gameManager = gameObject.GetComponent<GameManager>();
        Money = startMoney;
        Lives = startLives;
    }
    void Update(){
        if(Input.GetKey(KeyCode.M)){
            Money += 100;
        }
        if (Lives <= 0) {
            gameManager.EndGame();
        }
    }
    public void HitLives() {
        Lives--;
        LivesTxt.text = Lives.ToString()+" LIVES";
    }
}
