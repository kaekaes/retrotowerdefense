﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LootBox : MonoBehaviour{

    // quantDeDrop es la variable que controla la cantidad de cartas dropeadas si o si.
    // sobresMenu es el contenedor de todos los sobres.
    // canvas es el grupo donde se meten las cartas que salen de un sobre
    // canvasmenu es el objeto donde está el objeto canvas
    // shopmenu es el objeto padre de toda la tienda
    // returnbutton es la flecha para volver
    // YesNoPanel es el prefab del panel de seleccion de si quieres comprar o no
    // YesNoPanelInst es una var donde se almacena el panel actual
    // Playergenerals es el script donde está el dinero actual del jugador
    // coste es el precio de este sobre
    // TextoVoid es un objeto de texto vacío para instantiarlo y colocarlo
    // yesnoButton es la var que coge el boton si y no del objeto yesnopanelinst
    public int cantidadadropear;
    public GameObject sobresMenu;
    public GameObject canvas;
    public GameObject canvasmenu;
    public GameObject shopmenu;
    public GameObject returnButton;
    public GameObject YesNoPanel;
    public GameObject textovoid;
    private GameObject YesNoPanelInst;
    public PlayerGenerals playerGenerals;
    public int coste;
    private Button[] yesno_Button = new Button[2];

    // Con este trozo de script creas un objeto que contiene el nombre que será mostrado en la lista de cartas posibles del
    // sobre, el objeto que debe de instanciar si toca y con que porcentaje saldrá, tambien tiene la lista con las rarezas
    [System.Serializable]
    public class LootCard {
        public string NombreCarta;
        public int probabilidad;
        public List<Rarity> rare = new List<Rarity>();
    }
    // 
    [System.Serializable]
    public class Rarity {
        public string NameRarity;
        public GameObject cardGO;
        public int probabilidad;
    }
    // La lista con todas las cartas posibles del sobre
    public List<LootCard> Drop = new List<LootCard>();
    
    // La accion de seleccionar una cantidad de cartas X aleatorias de entre la lista "Drop"
    public void LootAction() {
        int dropDeCarta = cantidadadropear;
        while (dropDeCarta > 0) {
            foreach (var carta in Drop) {
                int chance = Random.Range(1, 101);
                if (dropDeCarta > 0) {
                    if (chance < carta.probabilidad) {
                        var dropDeRareza = 1;
                        while (dropDeRareza > 0) {
                            foreach (var cartar in carta.rare) {
                                int chancer = Random.Range(1, 101);
                                if (dropDeRareza > 0) {
                                    if (chancer < cartar.probabilidad) {
                                        //Debug.Log("El numero de drop es: " + chance.ToString() + ". el numero de rareza es: " + chancer.ToString());
                                        Instantiate(cartar.cardGO, canvas.transform);
                                        dropDeCarta--;
                                        dropDeRareza--;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return;
    }

    // Crear el menu de si o no al clicar un sobre y asignarle a los botones de si y no sus acciones
    // deshabilita el boton de volver al menu principal
    public void YesNo() {
        if(YesNoPanelInst != null){
            return;
        }
        YesNoPanelInst = Instantiate(YesNoPanel,shopmenu.transform);

        foreach (Transform tr in YesNoPanelInst.transform) {
            if (tr.name == "Yes") {
                yesno_Button[0] = tr.gameObject.GetComponent<Button>();
            } else if (tr.name == "No") {
                yesno_Button[1] = tr.gameObject.GetComponent<Button>();
            }
        }

        //textopos es para saber donde debe crear los textos con los nombres de las cartas
        //posy es la posicion en Y de cada texto.

        var textopos = YesNoPanelInst.transform.GetChild(3);
        int posy = 0;
        int cardsinlist = Drop.Count;
        int posymax = Mathf.RoundToInt((Screen.height*0.80f) / 2);

        foreach (var carta in Drop) {
            var cosa = Instantiate(textovoid, textopos);
            cosa.transform.position = new Vector3(cosa.transform.position.x, cosa.transform.position.y - posy, cosa.transform.position.z);
            cosa.GetComponent<Text>().text = carta.NombreCarta;
            posy += posymax / cardsinlist;
        }
        returnButton.SetActive(false);
        yesno_Button[0].onClick.AddListener(Yes);
        yesno_Button[1].onClick.AddListener(No);

    }

    // acciones del boton si
    // Si el dinero es suficiente para comprar el sobre, 
    //te quita el dinero y lo actualiza de la pantalla (tambien lo guarda en datos del jugador)

    // activa el menú donde se ven las cartas del sobre y desactiva los sobres, inhabilita los botones si y no y asigna al
    // boton "ok" sus acciones, despues hace LOOTACCTION()
    public void Yes() {
        if (coste <= playerGenerals.torres) {
            YesNoPanelInst.SetActive(false);
            playerGenerals.torres -= coste;
            canvasmenu.SetActive(true);
            sobresMenu.SetActive(false);
            yesno_Button[0].interactable = false;
            yesno_Button[1].interactable = false;
            canvasmenu.GetComponent<Button>().onClick.AddListener(DeleteShopCards);
            LootAction();
            playerGenerals.UpdateMoney();
            playerGenerals.Save();
        }
    }

    // elimina el menu de si y no y reactiva el boton de volver al menu principal
    public void No() {
        Destroy(YesNoPanelInst);
        returnButton.SetActive(true);
    }

    // elimina todas las cartas del sobre para que no estorben a la siguiente vez que se abra un sobre,
    // desactiva el menu de cartas de sobre y activa el de los sobres.
    // Habilita el boton de volver al menu principal y destruye el menu de si o no.
    public void DeleteShopCards() {
        foreach (Transform tr in canvas.transform) {
            Destroy(tr.gameObject);
        }
        canvasmenu.SetActive(false);
        sobresMenu.SetActive(true);
        returnButton.SetActive(true);
        Destroy(YesNoPanelInst);
    }
}
