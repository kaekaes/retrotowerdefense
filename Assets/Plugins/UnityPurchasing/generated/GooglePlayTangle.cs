#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("eFt+w1wpHJyFozPPopH1ndjDYRMCUQqdbZz1EkP6L0DJ+WlbEmwPUKQMGNFbIyelFPv+SQsnOi8nFeUBoyAuIRGjICsjoyAgIbzdSlpETjU0G6IQX/gX8yjNm6veakpCSDTnlEE8bs1vfdLQ2GqBSNFByFC/p6iCe735URFhDWX/S/7dl9nqB4CN4N29z98cRXN9C5nEMF5hCbpoIRa9byCKC8zS07wFb2nPuA40Yk8P9/JkLXhta8P5cl/tjmBT1KIbArdKSLwRoyADESwnKAunaafWLCAgICQhIu0GrZg5/VJ7qS5LXcYfG2VEo3692C+yW5XEuCGlzk40yJKxZgk4Pfj5tv0RNBcgMyF1IMxeng6S5lsb05KEWpE7MycxQCMiICEg");
        private static int[] order = new int[] { 5,9,2,9,12,12,11,10,13,13,12,13,13,13,14 };
        private static int key = 33;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
